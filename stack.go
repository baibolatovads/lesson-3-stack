package main

import "fmt"

type Stack []int

func (stack * Stack) Push(val int) string{
	*stack = append(*stack, val)
	return "OKAY!"
}

func (stack * Stack) Pop()int{
	if len(*stack) == 0{
		return 0
	} else {
		ind := len(*stack) - 1
		res := (*stack)[ind]
		*stack = (*stack)[:ind]
		return res
	}
}

func (stack * Stack) top()int{
	return (*stack)[0]
}

func (stack *Stack) getMax()int{
	max := (*stack)[0]
	for i:=0; i < len(*stack); i++{
		if (*stack)[i] > max {
			max = (*stack)[i]
		}
	}
	return max
}


func main(){
	var stack Stack
	stack.Push(5)
	stack.Push(7)
	fmt.Println(stack.top())
	fmt.Println(stack.getMax())
	fmt.Println(stack.Pop())
}