package main

import(
	"fmt",
	"time"
) 

func main() {

	//note the start time of the algorithm
	start := time.Now()
	
	//array that we want to test
	arr := [5]int{5,3,4,2,1}

	for i := 0; i < len(arr); i++ {  
		for j := 0; j < len(arr); j++ {
			if arr[i] < arr[j] {
			//we make swap if next element is bigger that previous element 
				temp := arr[j]  
				arr[j] = arr[i]
				arr[i] = temp
			}
		}
	}

	for i := 0; i < len(arr); i++ {
		fmt.Println(arr[i])
	}

	//we note the time when algorithm is ended and 
	//find the difference between start and end time to calculate complexity
	t := time.Now()  //O(n^2)
	elapsed := t.Sub(start)
	fmt.Println(elapsed)
}