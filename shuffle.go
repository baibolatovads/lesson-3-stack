package main

import "math/rand"
import "fmt"

//we create a structure of Cards
type Cards struct {
	Rank string
	Suit string
}

//The Deck of Cards
type Deck[] Cards

//Function to generate new deck of cards
func NewDeck() (deck Deck){
	ranks := []string{"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight",
		              "Nine", "Ten", "Jack", "Queen", "King"}
	suits := []string{"Diamonds", "Hearts", "Spades", "Clubs"}

	for i := 0; i < len(ranks); i++{
		for j := 0; j < len(suits); j++{
			card := Cards{
				Rank: ranks[i],
				Suit: suits[j],
			}
			deck = append(deck, card)
		}
	}
	return
}

//Function to shuffle deck of cards
func Shuffle(deck Deck) Deck{
	for i:= 0; i < len(deck); i++{
		r := rand.Intn(i + 1)
		if i != r{
			deck[r], deck[i] = deck[i], deck[r]
		}
	}
	return deck
}

func main(){
	deck := NewDeck()
	fmt.Println(Shuffle(deck))
}